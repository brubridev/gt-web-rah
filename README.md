# RAH - Rede de Apoio das e nas Periferias

## Quem é a RAH?

A Rede de Apoio Humanitário (RAH) é uma iniciativa dos movimentos negros e periféricos do munícipio de São Paulo para realizar ações emergências de forma conjunta e organizada. O objetivo é somar ao movimento de minimização de impactos negativos ocasionados pela pandemia do novo coronavírus.
O desejo da rede é atender as necessidades básicas de sobrevivência das famílias periféricas que se encontram em dificuldades financeiras nesse período de crise sanitária, tais como: Alimentação e Higiene Pessoal.

As ações principais da rede são:

-     Identificar as famílias mais vulneráveis;
-     Mapear os Pontos de Entrega de doações;
-     Articulação em rede; e
-     Criar estratégias de distribuição dos alimentos e dos produtos de higiene pessoal.

Com isso a rede consegue realizar o propósito dessa organização, que nasceu para conectar a pessoa doadora com pessoa que precisa receber a doação.

<details>
  <summary>ORGANIZAÇÕES PARCEIRAS</summary>
  
  ## ORGANIZAÇÕES PARCEIRAS
1. Periferia é o Centro
2. Frente Favela Brasil
3. Rede Geração Solidária
4. Periferia é o Centro
5. FUNANI
6. INTERCAB-SP
7. Rede Quilombação
8. Nova Frente Negra Brasileira
9. Articulador@s Periféric@s Independentes

</details>

<details>
  <summary>POLOS PARCEIROS</summary>
  
  ## POLOS PARCEIROS
1. . Escola de País do Brasil
2. Associação de Melhoramentos do Sítio de Campo
3. Sociedade de Melhoramentos do Monte Serrat
4. Imperatriz Alvinegra
5. Ação Solidária Adventista
6. Ação Beneficente Menina dos Olhos de Ouro
7. Associação de Mulheres Luta pra Valer
8. Carmelita Mineiro
9. Casa de Oração Jardim Maringá
10. Comitê de Luta da Vila Nhocune
11. Comunidade Casa do Trem
12. Maravilha 2
13. OSC Izaías Luzia da Silva
14. ALMEM
15.	Amavb Associação
16.	Apajé
17.	Associação para o Desenvolvimento Social e Sustentável (ADESS) 
18.	Associação Todos Unidos Abraços Íris
19.	Casa Cultural Hip Hop Jaçanã
20.	I.Q.C. Instituto de Qualificação e Cidadania
21.	Instituto Saci–Saberes Cultura e Integração
22.	Ocupação Nova Paris
23.	Rede Geração Solidária
24.	A Voz dos Inocentes
25.	Associação dos Moradores da Ponta da Praia
26.	Associação dos Moradores Vila Boa Esperança
27.	Associação União dos Moradores Ponto de Areia e Adjacências 
28.	Associação Vila Nova Esperança/Sem Terra_Extremo Zona Oeste
29.	Condô Cultural
30.	Grupo Espaço Cultural Cachoeiras
31.	Ilé Axé Abassá do Oxóssi Comunidade Caramqzal Apolopi
32.	Mulher de Fé
33.	Obadias
34.	ONG Projeto Alavanca Brasil
35.	Tatiane Camargo (Pessoa Física)
36.	União dos Moradores do Jardim Jaqueline
37.	Arco Associação Beneficente
38.	Igreja Batista
39.	Tetto
40.	INTECAB-SP
41.	Nega Odara Mulheres
42.	Associação Amigos do Recreio São Jorge 2
43.	Ação Social de Apoio aos Moradores do Cantinho do Céu
44.	Associação de Moradores de Bairro da Vila Rubi
45.	Associação dos Moradores Jardim Clipper
46.	Bloco do Beco
47.	Chácara do Sol
48.	Comunidade Santos Cidade Dutra
49.	Instituto CIGANO do Brasil
50.	Instituto Herdeiros do Futuro
51.	Instituto Herdeiros do Futuro_SPVV Jardim São Luís
52.	Josefina Bakhita
53.	Lágrimas das Pretas
54.	ReggArte
55.	Sociedade Amiga Esportiva do Jardim Colacabana
56.	Sorriso de Criança
57.	SPVV Santo Amaro
58.	Vivi Baixadas
59.	Comunidade Evangélica Moriah
60.	Comunidade Moriaj – Jardim Vera Cruz
61.	Associação São Vicente de Paulo
62.	CCA Juntos I
63.	Espaço de Acolhimento Tia Estela
64.	Associação Semente
65.	Casa AmarEla Quilombo Afroguarany
66.	Associação de Moradores do Movimento Anchieta
67.	Organização Terra Prometida
68.	Ação Comunitária Recanto Cocaia
69.	Associação de Bairro Casa Branca
70.	Coletivo Ponto a Ponto
71.	Fundação Julita
</details>

## GT-WEB-RAH

Projeto de desenvolvimento de aplicativo Web responsivo da Rede de Apoio nas e das Periferias.

## Como colaborar no projeto

Esse é um projeto open-source, mas gerenciado pelo Grupo de Tecnologia e Geoprocessamento da Rede. Qualquer pessoa pode contribuir com código, revisões ou ideias para finalizarmos o projeto.

### QUANTIDADE DE CESTAS JÁ ENTREGUES POR REGIÃO

- ZONA LESTE: 71
- ZONA NORTE: 72
- ZONA OESTE: 90
- ZONA SUL: 52
- CENTRO: 30
- GUARULHOS: 30
- IBITINGA: 30

TOTAL: 375 CESTAS BÁSICAS ENTREGUES
Dados atualizados em: Maio/2020.
